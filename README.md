# Bitcoin Cash Glossary - Glosario Bitcoin Cash

An English-Spanish glossary for the benefit of the cryptosphere in general, the Bitcoin Cash community in particular, and any biological or cybernetic form somehow interested in it.

1. [Preface](https://gitlab.com/leandrodimarco/bitcoin-cash-glossary/wikis/Preface)
2. [Explanation](https://gitlab.com/leandrodimarco/bitcoin-cash-glossary/wikis/Explanation)
3. [English to Spanish](https://gitlab.com/leandrodimarco/bitcoin-cash-glossary/wikis/English-to-Spanish) - *In progress*


Un glosario Español-Inglés para el beneficio de la criptoesfera en general, la comunindad Bitcoin Cash en particular, y cualquier forma biológica o cibernéetica de alguma manera interesada en ello.

1. Prefacio - *En breve*
2. Explicación - *En breve*
3. Español a Inglés - *En breve*
